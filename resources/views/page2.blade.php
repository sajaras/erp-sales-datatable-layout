@extends('erp-layout.sitelayout')


@section('thispageleftsidebar')

<!-- left navigation bar -->
      <nav class="navbar navbar-inverse erpsidebar schemecolor3bg navbar-fixed-left" style="top:8%; z-index:10;border:none;" >
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header ">
      <button type="button" class="pull-left navbar-toggle collapsed schemecolor1bg" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <!-- <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> -->
        <span><i class="fa fa-chevron-circle-down schemecolor2fg" aria-hidden="true"></i>
</span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav text-center">
        <li class="active"><a href="#">MANAGE ORGANISATION <span class="sr-only">(current)</span></a></li>
          <li ><a href="#" class="sidebarlink schemecolor1fg">ADDRESS BOOK</a></li>
      <li class="accordion">ADDRESS MASTERS<span><i style="margin-left:5px;" class="fa fa-sort-desc fa-1x"  aria-hidden="true"></i></span></li>
  <div class="panel text-center schemecolor2bg schemecolor1fg">
    <li style="padding:10px;">MANAGE STATES</li>
    <li style="padding:10px;">MANAGE DISTRICTS</li>
    <li style="padding:10px;">MANAGE TALUKS</li>
    <li style="padding:10px;">MANAGE BLOCKS</li>
    <li style="padding:10px;">MANAGE PANCHAYATHS</li>
    <li style="padding:10px;">MANAGE WARDS</li>
  </div>




      </ul>


    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- end of left navigation bar -->

@endsection

@section('thispagehtml')
<br>
<br>
<br>
<div class="row">
  <div class="col-sm-offset-1 col-sm-10">
<table id="magictable" class="table table-striped table-bordered table-hover display">
    <thead>
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Discount</th>
        </tr>
    </thead>
    <tbody>
      @foreach($products as $eachproduct)
      <tr>
        <td>{{$eachproduct->id}}</td>
        <td>{{$eachproduct->name}}</td>
        <td>{{$eachproduct->price}}</td>
        <td>{{$eachproduct->stock}}</td>
        <td>{{$eachproduct->discount}}</td>
      </tr>
      @endforeach
    </tbody>
</table>
</div>
</div>
@endsection

@section('thispagejavascript_jquery')
<script type="text/javascript">
$(document).ready(function(){
var table = $('#magictable').DataTable({
  paging:false,
  scrollY:300,
  dom: 'B<"clear">l<"row"<"col-sm-offset-3 col-lg-4"f>>rtip',
  select:true,
  buttons: ['copy',{extend:'excel',className:'schemecolor2bg schemecolor1fg',text:'Export to Excel'},'print'],
  processing:true,
  responsive:true,
  "columnDefs": [
 { "searchable": false, "targets": 1 }
                ],

});
});
</script>
@endsection
